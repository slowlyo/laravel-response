<?php


namespace Slowlyo\LaravelResponse;

use Illuminate\Http\Resources\Json\JsonResource;
use  \Illuminate\Http\Response as HttpResponse;

class WsJsonResponse
{
    public static function userNotFound($type, $message = 'User Not Found')
    {
        return self::fail($type, $message, HttpResponse::HTTP_NOT_FOUND);
    }

    public static function errorNotFound($type, $message = 'Not Found')
    {
        return self::fail($type, $message, HttpResponse::HTTP_NOT_FOUND);
    }

    /**
     * @param string $type
     * @param string $message
     * @param int $code
     * @param null $data
     * @return false|string
     */
    public static function fail(string $type, string $message = '', int $code = HttpResponse::HTTP_INTERNAL_SERVER_ERROR, $data = null)
    {
        $status = ($code >= 400 && $code <= 499) ? 'error' : 'fail';
        $message = $message ?: (HttpResponse::$statusTexts[$code] ?? 'Service error');

        return json_encode([
            'type' => $type,
            'error' => true,
            'status' => $status,
            'code' => $code,
            'message' => $message,// 错误描述
            'data' => (object)$data,// 错误详情
        ], JSON_UNESCAPED_UNICODE);
    }

    public static function errorParam($type, $message = 'Param Error', $data=null)
    {
        return self::fail($type, $message, HttpResponse::HTTP_BAD_REQUEST, $data);
    }

    public static function errorPermission($type, $message = 'Permission Error', $data=null)
    {
        return self::fail($type, $message, HttpResponse::HTTP_FORBIDDEN, $data);
    }

    public static function errorBadRequest($type, $message = 'Bad Request', $data=null)
    {
        return self::fail($type, $message, HttpResponse::HTTP_BAD_REQUEST, $data);
    }

    public static function errorForbidden($type, $message = 'Forbidden')
    {
        return self::fail($type, $message, HttpResponse::HTTP_FORBIDDEN);
    }

    public static function errorUnauthorized($type, $message = 'Unauthorized')
    {
        return self::fail($type, $message, HttpResponse::HTTP_UNAUTHORIZED);
    }

    /**
     * @param JsonResource|array|null $data
     * @param string $message
     * @param array $otherData
     * @param int $code
     * @return false|string
     */
    public static function success($type, $data=null, string $message = '', $otherData=[], $code = HttpResponse::HTTP_OK)
    {
        $message = $message ?: (HttpResponse::$statusTexts[$code] ?? 'OK');
        $additionalData = [
            'type' => $type,
            'error' => false,
            'status' => 'success',
            'code' => $code,
            'message' => $message
        ];
        if ($otherData) {
            $additionalData = array_merge($additionalData, $otherData);
        }
        if ($data instanceof JsonResource) {
            return $data->additional($additionalData)->toJson();
        }
        if ($data === null) {
            $data = (object)$data;
        }
        return json_encode(array_merge($additionalData, ['data' => $data]), JSON_UNESCAPED_UNICODE);
    }
}
