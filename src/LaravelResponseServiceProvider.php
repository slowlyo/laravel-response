<?php

namespace Slowlyo\LaravelResponse;

use Illuminate\Support\ServiceProvider;

class LaravelResponseServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->singleton('response-plus', function (){
            return new JsonResponse();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(): void
    {

    }
}
