<?php

namespace Slowlyo\LaravelResponse;

use Illuminate\Support\Facades\Facade;

/** @mixin JsonResponse */
class JsonResponseFacade extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'response-plus';
    }
}
